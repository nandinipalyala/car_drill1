// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


let inventory = require('./data')

function sortingcars(){
    for(let index = 0; index < inventory.length; index++){
        for(let index1 = index+1; index1 < inventory.length; index1++){
            if(inventory[index].car_model > inventory[index1].car_model){
                let temp = inventory[index]
                inventory[index] = inventory[index1];
                inventory[index1] = temp;
            }
        }
    }
    return inventory;
}

module.exports = sortingcars;
