// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


let inventory = require('./data')

function findCars(){
    let carslength = [];
    for(let older of inventory){
        if(older.car_year < 2000){
            carslength.push(older)
        }
    }
    let olderCars = carslength.length;
    console.log(`The olderCars length is ${olderCars}`);
    return carslength;
}

module.exports = findCars;