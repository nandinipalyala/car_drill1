// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only
// contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

let inventory = require('./data')

function FindCarmaker(carmake1, carmake2){
    let carmakers = [];
    for(let make of inventory){
        if(make.car_make == carmake1|| make.car_make == carmake2){
            carmakers.push(make)
        }
    }
    return carmakers;
}

module.exports= FindCarmaker;